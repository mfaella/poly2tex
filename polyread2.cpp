#include "main.h"
#include "polyread.h"

enum Operator {LL, LE, GG, GE, EE};

void skip_blanks(std::string::iterator& it)
{
  while (*it == ' ') it++;
}

Operator read_op(std::string::iterator& it)
{
  Operator ret;
  
  skip_blanks(it);
  char sym = *it;
  switch(sym)  {
  case '<': 
    if (*(it+1) == '=') {
      ret = LE;
      it++;
    }
    else 
      ret = LL;
    break;
  case '>': 
    sym = *it;
    if (*(it+1)=='=') {
      ret = GE;
      it++;
    }
    else 
      ret = GG;
  case '=':
    if (*(it+1)=='=') {
      ret = EE;
      it++;
    }
    break;
  }
  it++;
  return ret;
}

Coefficient read_coefficient(std::string::iterator& it)
  {
    bool negative = false;
    Coefficient coeff = 0;
    char sym;
    
    skip_blanks(it);
    sym = *it;
    if (isdigit(sym)) {
      //      cerr << "Symbolo " << sym << " = " << *(it) << endl;
      while(isdigit(sym))  {
	coeff = coeff*10 + (sym-'0');
	//	cerr << "Next " << sym << *(it+1) << endl;
	sym = *(++it);
      }
    }
    else coeff = 1;
    cerr << "Coeff " << coeff << endl;
    if (sym=='*') ++it;
    return(coeff);
  }

Linear_Expression read_var(std::string::iterator& it, Varmap vars)
  {
    string sym;
    int endsym=0;

    skip_blanks(it);
    while (isalnum(*it+endsym))
      endsym++; 
    sym = string(*it,endsym);
    cerr << "Var is " << sym << " with index " << vars[sym] << endl;
    return(Linear_Expression(Variable(vars[sym])));
  }

Linear_Expression read_linterm(std::string::iterator& it, Varmap vars)
  {
    Linear_Expression ris=Linear_Expression::zero();
    Coefficient coeff;

    coeff = read_coefficient(it);
    if (isalpha(*it)) {
      ris = ris + coeff*read_var(it, vars);
    }
    else 
      ris = ris + coeff;
    return(ris);
	}

Linear_Expression read_linexp(std::string::iterator& it, std::string::iterator end, Varmap vars)
  {
    Linear_Expression ris=Linear_Expression::zero();
    char sym;
    bool done = false;
 
    while (!done) {
      skip_blanks(it);
      sym = *it;
      int a = (int) sym;
      // cerr << "NEXT LINEXP " << sym << " = " << a << " ." << endl;
      if (sym != '<' && sym != '>' && sym !='=' && sym !=',' && sym !=13 && it != end) {
	switch(sym) {
	case '-' : 
	  it++;
	  ris = ris - read_linterm(it,vars);
	  break;
	case '+' : 
	  it++;
	  ris = ris + read_linterm(it,vars);
	  break;
	default: 
	  ris = read_linterm(it,vars);
	  break;
	}
      }
      else 
				done = true;
    }
    return ris;
  }


void polyhedron_add_constr(Poly& ph,Linear_Expression e1, Operator op, Linear_Expression e2)
{
	
  switch(op) {
  case LL: 
    ph.add_constraint(e1 < e2);
    break;
  case LE: 
    ph.add_constraint(e1 <= e2);
    break;
  case GG: 
    ph.add_constraint(e1 > e2);
    break;
  case GE: 
    ph.add_constraint(e1 >= e2);
    break;
  case EE: 
    ph.add_constraint(e1 == e2);
    break;
  }
}

void goto_start(std::string::iterator& it)
{
	while (*it != ':') it++;
	it++;
}

Poly read_polyhedron(string str, Varmap vars,dimension_type dim)
{
  std::string::iterator it=str.begin();
  std::string::iterator it_end=str.end();
  Linear_Expression expr1,expr2;
  Operator op;
  bool done=false;
  NNC_Polyhedron ph(dim);
  
  goto_start(it);
  cerr << "Next chars " << *it << *(it+1) << endl;
  while(!done) {
    expr1 = read_linexp(it,it_end, vars);
    cerr << "Linexp done." << endl;
    op = read_op(it);
    cerr << "OP Done." << endl;
    expr2 = read_linexp(it,it_end,vars);
    cerr << "Linexp done." << endl;
    polyhedron_add_constr(ph,expr1,op, expr2);
    skip_blanks(it);
    if (*it != ',' || it>=it_end) 
      done=true;
    it++;
  }
  return ph;
}

/* Obsolete:

Poly settozero(Variables_Set varset)
{
  Variables_Set::iterator it;
  Linear_Expression e;
  Poly ph(3,UNIVERSE);
	
  for(it = varset.begin(); it != varset.end(); it++) {
    e = Linear_Expression(Variable(*it));
    ph.add_constraint(e == 0);
  }
  return ph;
  }
*/

Poly section(Variables_Set varset, Sectionmap section_values)
{
  Variables_Set::iterator it;
  Linear_Expression e;
  Poly ph(3,UNIVERSE);
	
  for(it = varset.begin(); it != varset.end(); it++) {
    e = Linear_Expression(Variable(*it));
    // fixed resolution
    int denum = 100, num;
    num = (int) (section_values[*it] * 100);
    if (num==0)
      cerr << "Warning: section was rounded to 0." << endl;
    
    ph.add_constraint(denum * e == num);
  }
  return ph;
}

PPS read_powerset(std::fstream& file, Varmap vars, Variables_Set varset, Sectionmap section_values)
{
  dimension_type dim = (dimension_type) vars.size();
  Poly poly(dim,EMPTY);
  PPS ps(poly);
  PPS sectioned_vars(dim,EMPTY);
  int j=0;
	
  cerr << "Dimension is " << (int)dim << "\n";
  if (file) 
    while(!file.eof()) {
      //      while(!file.eof());
      //      j++;
      //      if (!file.eof()) {
      std::string str;
      getline(file,str);
      if (str.size()>0) {
	cerr << "\nLinea: " << str.c_str() << endl;
	Poly  poly = read_polyhedron(str, vars, dim);
	cerr << "Poliedro: " << flush;
	poly.print();
	ps.add_disjunct(poly);
      }
    }
  cerr << "FINAL1 ";

  if (varset.size() > 0) {
    sectioned_vars.add_disjunct(section(varset, section_values));
    // cerr << "Letti " << j << " poliedri.\n";
    // cerr << "POWERSET: ";
    //   ps.print();
    //   cerr << "\n";  
    // cerr << "ZEROED: ";
    // zeroed.print();
    // cerr << "\n";  
    cerr << "-Intersection (" << ps.size() << ")-" << flush;
    ps.intersection_assign(sectioned_vars);
    cerr << "-Remove space dimensions (" << ps.size() << ")-" << flush;
    ps.remove_space_dimensions(varset);
  }
  cerr << endl << "FINAL: ";
  ps.print();
  cerr << "\n";  
  return ps;
}
