#include <vector>
#include <cmath>

#include "main.h"
#include "tikz-plot.h"

static bool compare_angle(const vector<float>& first, const vector<float>& second) {
  return first[2] < second[2];
}


/* 
 * relative_interior restituisce il relative interior del poliedro preso in input
 * 
 *  INPUT:   ph 			(NNC_Polyhedron)	-> poliedro a partire dal quale calcolare 
 * 					   	   il relative interior;
 *  OUTPUT:  ph_relative_interior	(NNC_Polyhedron)	-> relative interior di ph;
 * 
 */
static Poly relative_interior(const Poly &ph) 
{
  //sistema di vincoli di ph
  const Constraint_System& cs = ph.constraints();
  //sistema di vincoli del relative interior di ph
  Constraint_System new_cs;
  //cs.set_space_dimension(ph.space_dimension());
  
  for (Constraint_System::const_iterator i = cs.begin(), cs_end = cs.end(); i != cs_end; ++i)
  {
    Constraint c=*i;
    //se sono vincoli > o = vengono aggiunti così come sono a new_cs
    if (c.is_strict_inequality()||c.is_equality())
      new_cs.insert(c);
    else
    //altrimenti  
    //si tratta di un vincolo >= che viene convertito in >
    {
#if PPL_VERSION_MAJOR == 0
      Linear_Expression e = Linear_Expression(c);
#else
      Linear_Expression e(c.expression());
#endif
      Constraint strict_ineq_c=(e>0);
      new_cs.insert(strict_ineq_c);
    }	 
  }
  
  Poly ph_relative_interior(new_cs);
  return ph_relative_interior;
}

/* Shrinks topologically open sides of a polyhedron. */
static Poly shrink_polyhedron(const Poly &p, float amount)
{
  int inverse = (int) (1/amount);

  //sistema di vincoli di ph
  const Constraint_System& cs = p.constraints();
  //sistema di vincoli del nuovo poliedro
  Constraint_System new_cs;
  //cs.set_space_dimension(ph.space_dimension());
  
  for (Constraint_System::const_iterator i = cs.begin(), cs_end = cs.end(); i != cs_end; ++i) {
    Constraint c = *i;
    //se sono vincoli >= o = vengono aggiunti così come sono a new_cs
    if (c.is_nonstrict_inequality() || c.is_equality())
      new_cs.insert(c);
    else
    //altrimenti si tratta di un vincolo >, che viene rafforzato
    {
#if PPL_VERSION_MAJOR == 0
      Linear_Expression e = Linear_Expression(c);
#else
      Linear_Expression e(c.expression());
#endif
      Constraint strict_ineq_c = (inverse * e > 1);
      new_cs.insert(strict_ineq_c);
    }	 
  }
  
  return Poly(new_cs);
}


/* Plots a single convex polyhedron.
 */
static void plot(Poly p, int counter)
{
  if (p.is_empty()) return;
  
  p = shrink_polyhedron(p, 0.1);

  Generator_System const &gen_sys = p.minimized_generators();
#if PPL_VERSION_MAJOR == 0
  Generator_System::const_iterator it;
#else
  Generator_System_const_iterator it;
#endif

  string tag = "AREA"; // the tikz style for the shapes
  Variable x(0), y(1);

  Poly interior = relative_interior(p);
  
  /* Each row will contain 3 floats:
      - the first two are the coordinates of the vertices;
      - the third is the angle between the current vertex and an interior point.
     This is a shortcut to write a simpler comparator without using a lambda function (C++11)
  */
  vector< vector<float> > coords;
  float avg[2] = {0, 0}; // coordinates of an interior point

  for (it = gen_sys.begin(); it != gen_sys.end(); it++) {
    Generator gen = *it;
    if (gen.is_closure_point() ||
	(gen.is_point() && interior.relation_with(gen) != Poly_Gen_Relation::subsumes()) ) {
      
      GMP_Integer gen_x = gen.coefficient(x),
                  gen_y = gen.coefficient(y),
	          gen_divisor = gen.divisor();

      mpq_class rational_x(gen_x, gen_divisor),
	        rational_y(gen_y, gen_divisor);
	
      float point_x = mpq_get_d(rational_x.get_mpq_t()),
	    point_y = mpq_get_d(rational_y.get_mpq_t());

      // cout << "*** (" << gen_x << ", " << gen_y << ") / " << gen_divisor << " -> ";
      // cout << "(" << point_x << ", " << point_y << ")" << endl;

      vector<float> temp;
      temp.push_back(point_x);
      temp.push_back(point_y);
      coords.push_back(temp);
      avg[0] += point_x;
      avg[1] += point_y;
    }
  }
   
  int len = coords.size();
  avg[0] /= len;
  avg[1] /= len;

  // cout << "DEBUG: " << avg[0] << ", " << avg[1] << endl;

  for (int i=0; i<len ;i++) {
    float diff_x = coords[i][0] - avg[0],
          diff_y = coords[i][1] - avg[1],
          angle = atan2(diff_y , diff_x);
    coords[i].push_back(angle);
  } 

  /* cout << "$$$" ;
   for (int i = 0; i < len; ++i) {
    for (int j = 0; j< coords[i].size(); ++j)
      std::cout << coords[i][j] << ' ';
    cout << endl ;
    } */

  // sort according to angle with center
  sort(coords.begin(), coords.end(), compare_angle);

  /* cout << "$$$" ;
  for (int i = 0; i < len; ++i) {
    for (int j = 0; j< coords[i].size(); ++j)
      std::cout << coords[i][j] << ' ';
    cout << endl ;
    } */
  
  if (len>=3) { // the polyhedron has an area
    cout << "\\fill[" << tag << counter << "] ";
    for (int i=0; i<len ;i++)
      cout << "(" << coords[i][0]   << ", " << coords[i][1]   << ") -- ";
    cout << "cycle;" << endl;
  } else { // a line or a point
    cout << "\\draw[" << tag << counter << "] ";
    for (int i=0; i<len ;i++) {
      cout << "(" << coords[i][0] << ", " << coords[i][1] << ")";
      if (i<len-1)
	cout << " -- ";
      else
	cout << ";";
    }
    cout << endl;
  }
}


void plot_init(int xmin, int xmax, int ymin, int ymax)
{
  Variable x(0), y(1); 

  float xtick = (xmax-xmin)/20.0,
        ytick = (ymax-ymin)/20.0,
        axis_xmin = xmin - xtick, // add 5% to each side
        axis_xmax = xmax + xtick,
        axis_ymin = ymin - ytick,
        axis_ymax = ymax + ytick;

  cout.precision(3);
  cout << "\\draw[->] (" << axis_xmin << ", 0) -- (" << axis_xmax << ", 0);\n";
  cout << "\\draw[->] (0, " << axis_ymin << ") -- (0, " << axis_ymax << ");\n";
  cout << "\\foreach \\x in {" << xmin << ",...," << xmax << "}  \\draw (\\x,-3pt) -- (\\x,3pt);\n";
  cout << "\\foreach \\y in {" << ymin << ",...," << ymax << "}  \\draw (-3pt,\\y) -- (3pt,\\y);\n";
  cout << "\\draw (" << axis_xmax + xtick << ", 0) node {$x$};\n";
  cout << "\\draw (0, " << axis_ymax + ytick << ") node {$y$};\n";
}


void plot(PPS &region)
{
  static int counter = 0;

  counter++;
  for (PPS::iterator it = region.begin(); it != region.end(); it++)
    plot(it->pointset(), counter);   
}


