#include "main.h"
#include "tikz-plot.h"
#include "polyread.h"

// Before executing:
// export LD_LIBRARY_PATH=/home/mfaella/Projects/PPL/installed/lib
// Example usage:
// crop -7 7 -2 7 -f Start-4D.poly x1 y1 x2=6 y2=4

static const bool DEBUG = false;

void dump_map(Varmap& map) {
  for ( Varmap::const_iterator it = map.begin(); it != map.end(); it++) {
    cerr << "Key " << it->first;
    cerr << " has value " << it->second << endl;
  }
}


/* Returns a convex polyhedron which is the conjunction of constraints
   of the type x = c, for all x in varset and c in section_values.
 */
void section(PPS &powerset, Variables_Set varset, Sectionmap section_values)
{
  Variables_Set::iterator it;
  Linear_Expression e;
  const dimension_type dim = powerset.space_dimension();

  Poly ph(dim, UNIVERSE);
	
  for(it = varset.begin(); it != varset.end(); it++) {
    e = Linear_Expression(Variable(*it));
    // fixed resolution
    int denum = 100, num;
    num = (int) (section_values[*it] * 100);
    if (section_values[*it]!=0 && num==0)
      cerr << "Warning: section was rounded to 0." << endl;
    
    ph.add_constraint(denum * e == num);
  }

  PPS sectioned_vars(ph);
  if (DEBUG)
    cerr << "-Intersection (" << powerset.size() << ")-" << flush;
  powerset.intersection_assign(sectioned_vars);
  if (DEBUG)
    cerr << "-Remove space dimensions (" << powerset.size() << ")-" << flush;
  powerset.remove_space_dimensions(varset);
  // cerr << endl << "POWERSET: " << powerset << endl;
}


void crop(PPS &powerset, int xmin, int xmax, int ymin, int ymax)
{
  Poly bounding_box = Poly(2);
  Variable x(0), y(1); 
  bounding_box.add_constraint(x >= xmin);
  bounding_box.add_constraint(x <= xmax);
  bounding_box.add_constraint(y >= ymin);
  bounding_box.add_constraint(y <= ymax);

  PPS bounding_box_pps(bounding_box); // promotion to PPS
  powerset.intersection_assign(bounding_box_pps);
}


int main(int argc, char* argv[])
{
  Variables_Set varset;
  Varmap vmap;
  Sectionmap smap;
  bool flip = false; // whether to complement the polyhedron
  
  std::string path[10];
  int j = 0, xmin=-1, ymin= -1, xmax=30, ymax= 15, nfile=0;

  try {
    if (argc < 2) throw string("Too few arguments.");
  
    // cerr << "ARGS " << argc << "\n";
    for (int i = 1; i < argc; ++i) {
      std::string arg = argv[i];
      if (arg == "-f") {
	if (i+1 < argc)
	  path[nfile++] = argv[++i];
      } else if (arg == "-crop") {
	if (i+2 < argc) {
	  xmin = atoi(argv[++i]);
	  xmax = atoi(argv[++i]);
	  ymin = atoi(argv[++i]);
	  ymax = atoi(argv[++i]);
	  if (DEBUG) cerr << "Crop = " << xmin << " " << xmax << " "  << ymin << " " << ymax << endl;
	}
      } else if (arg == "-flip") {
	flip = true;
      } else { // argument of the type x1=2.5
	char *equal_sign = strchr(argv[i], '=');
	string var_name;
	if (equal_sign==NULL) {
	  var_name = argv[i];
	} else {
	  var_name = string(argv[i], equal_sign-argv[i]);
	  varset.insert(Variable(j));
	  char *ret;
	  smap[j] = strtof(equal_sign+1, &ret);
	  if (DEBUG) cerr << var_name << " = " << smap[j] << "\n";
	  if (ret==equal_sign+1) {
	    cerr << "Error in parameter " << argv[i] << endl;
	    throw NULL;
	  }
	}
	vmap[var_name] = j;
	j++;
      }
    } // end for

    // dump_map(vmap);
    
    std::fstream file;

    if (nfile==0)
      throw string("At least one input file must be specified.");
    
    plot_init(xmin, xmax, ymin, ymax);

    for (int i = 0; i<nfile ; i++) {
      //      PPS region(2, EMPTY);
      file.open(path[i].c_str(),ios_base::in);
      if (!file)
	throw string("Error opening file <"+ path[i] + ">.");
      PPS region = read_powerset(file, vmap);
      file.close();
      section(region, varset, smap);
      // At this point, only 2 dimensions left
      if (region.space_dimension()!=2)
	throw string("Exactly two variables should be left free.");
      
      if (flip) {
	PPS universe(2, UNIVERSE);      
	universe.difference_assign(region);
	swap(region, universe);
      }
      crop(region, xmin, xmax, ymin, ymax);
      region.pairwise_reduce();
      plot(region);
    }
    
  } catch (string s) {
    cerr << endl << s << endl << endl;
    cerr << "Usage: " << endl;
    cerr << "       " << argv[0] << " -crop <xmin> <xmax> <ymin> <ymax> [-flip] [-f <input_file>]+ <var_1>[=val_1] ... <var_n>[=val_n]" << endl << endl;
    cerr << " -flip: \t complements the region\n";
    cerr << " var_1,...,var_n: \t list of all the variables occurring in the input files\n";
    cerr << " xmin,xmax,ymin,ymax: \t integers\n val's: \t\t floats\n\n";
    cerr << " All variables must be given a value except two (which are plotted).\n\n";
    return 1;
  }    

  // cerr << path.c_str() << "\n";
  //  for (Varmap::iterator it = vmap.begin(); it != vmap.end(); it++) {
  //  	cerr << "Variable " << it->first << " has index " << it->second << "\n"; 
  //  }
    
  return 0;
}
  

