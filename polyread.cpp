#include "main.h"
#include "polyread.h"

enum Operator {LL, LE, GG, GE, EE};

void skip_blanks(std::string::iterator& it)
{
  while (*it == ' ') it++;
}

Operator read_op(std::string::iterator& it)
{
  Operator ret;
  
  skip_blanks(it);
  char sym = *it;
  switch(sym)  {
  case '<': 
    if (*(it+1) == '=') {
      ret = LE;
      it++;
    }
    else 
      ret = LL;
    break;
  case '>': 
    sym = *it;
    if (*(it+1)=='=') {
      ret = GE;
      it++;
    }
    else 
      ret = GG;
  case '=':
    if (*(it+1)=='=') {
      ret = EE;
      it++;
    }
    break;
  }
  it++;
  return ret;
}

Coefficient read_coefficient(std::string::iterator& it)
  {
    bool negative = false;
    Coefficient coeff = 0;
    char sym;
    
    skip_blanks(it);
    sym = *it;
    if (isdigit(sym)) {
      while(isdigit(sym))  {
	coeff = coeff*10 + (sym-'0');
	sym = *(++it);
      }
    }
    else coeff = 1;
    if (sym=='*') ++it;
    //    cerr << "Coefficiente = " << coeff << endl;
    return(coeff);
  }

Linear_Expression read_var(std::string::iterator& it, Varmap vars)
  {
    string sym;
    int endsym=0, i=0;

    skip_blanks(it);
    while (isalnum(*(it+endsym)))
      endsym++;
    //    cerr << "Symbol of length " << endsym << " is ";
    //    while (i < endsym) {
    //      cerr <<  *(it+i);
    //      i++;
    //    }
    //    cerr << endl;
    sym = string(&(*it), (size_t) endsym);
    //    cerr << "Var is " << sym << " with index " << vars[sym] << endl;
    it += endsym;
    return(Linear_Expression(Variable(vars[sym])));
  }


Linear_Expression read_linterm(std::string::iterator& it, Varmap vars)
{
  Linear_Expression ris=Linear_Expression::zero();
  Coefficient coeff;
  
  coeff = read_coefficient(it);
  if (isalpha(*it)) {
    ris = ris + coeff*read_var(it, vars);
  }
  else 
    ris = ris + coeff;
  return(ris);
}

Linear_Expression read_linexp(std::string::iterator& it, std::string::iterator end, Varmap vars)
{
  Linear_Expression ris=Linear_Expression::zero();
  char sym;
  bool done = false;
  
  while (!done) {
    skip_blanks(it);
    sym = *it;
    if (sym != '<' && sym != '>' && sym !='=' && sym !='&' && it < end) {
      switch(sym) {
      case '-' : 
	it++;
	ris = ris - read_linterm(it,vars);
	break;
      case '+' : 
	it++;
	ris = ris + read_linterm(it,vars);
	break;
      default: 
	ris = read_linterm(it,vars);
	break;
      }
    }
    else 
      done = true;
  }
  return ris;
}


void polyhedron_add_constr(Poly& ph,Linear_Expression e1, Operator op, Linear_Expression e2)
{
  
  switch(op) {
  case LL: 
    ph.add_constraint(e1 < e2);
    break;
  case LE: 
    ph.add_constraint(e1 <= e2);
    break;
  case GG: 
    ph.add_constraint(e1 > e2);
    break;
  case GE: 
    ph.add_constraint(e1 >= e2);
    break;
  case EE: 
    ph.add_constraint(e1 == e2);
    break;
  }
}

Poly read_polyhedron(string str, Varmap vars,dimension_type dim)
{
  std::string::iterator it=str.begin();
  std::string::iterator it_end=str.end();
  Linear_Expression expr1,expr2;
  Operator op;
  bool done=false;
  NNC_Polyhedron ph(dim);
  
  while(!done) {
    expr1 = read_linexp(it,it_end, vars);
    op = read_op(it);
    expr2 = read_linexp(it,it_end,vars);
    polyhedron_add_constr(ph,expr1,op, expr2);
    skip_blanks(it);
    if (*it != '&' || it>=it_end) 
      done=true;
    it++;
  }
  return ph;
}

/* Obsolete:
Poly settozero(Variables_Set varset, dimension_type dim)
{
  Variables_Set::iterator it;
  Linear_Expression e;
  Poly ph(dim,UNIVERSE);
	
  for(it = varset.begin(); it != varset.end(); it++) {
    e = Linear_Expression(Variable(*it));
    ph.add_constraint(e == 0);
  }
  return ph;
  }*/


PPS read_powerset(std::fstream& file, Varmap vars)
{
  dimension_type dim = (dimension_type) vars.size();
  PPS ps(dim, EMPTY);
  int j=0;
  
  //	cerr << "Dimension is " << (int)dim << "\n";
  if (file) 
    while(!file.eof()) {
      // look for next { or EOF
      while(file.get()!='{' && !file.eof()); 
      if (!file.eof()) {
	j++;
	std::string str;
	getline(file, str, '}');
	Poly  poly = read_polyhedron(str, vars, dim);
	ps.add_disjunct(poly);
	cerr << ".";
      }
    }
  cerr << endl << j << " polyhedra read.\n";
  return ps;
}
