# poly2tex #

Converts a (possibly non-convex) polyhedron into a Tikz drawing.

### Building ###

It depends on Parma Polyhedra Library (PPL), version >=1.2.
A non-standard path can be specified in the `Makefile`.
Then, run `make`.

### Usage ###

    poly2tex -crop 0 10 0 10 -f mypoly.poly x y t=0 > mypoly.tex

Then, include mypoly.tex into a LaTeX file within the following context:

```
#!latex
\begin{figure}[h]
\centering
\begin{tikzpicture}

\input{mypoly.tex}

\end{tikzpicture}
\caption{An automatic picture.}
\label{fig:automatic}
\end{figure}
```

For more usage information, run poly2tex with no arguments.
For more info on the proper latex context, check out the file "example.tex".

### Input format ###

A "poly" file should contain a possibly non-convex polyhedron,
in the following syntax.

    ({x>0 & x<2 & x+y<2 & t=0} {x>2 & x<5 & y>0 & t<x}) 

### Contributors ###

* Marco Faella, marfaella@gmail.com
* Massimo Benerecetti