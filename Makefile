.SUFFIXES:
.SUFFIXES: .cpp .o

PPLPATH-STANDARD=/usr/local

# Modify according to the local settings
PPLPATH=$(PPLPATH-STANDARD)

CXXFLAGS=-ggdb -I$(PPLPATH)/include -L$(PPLPATH)/lib 
LIBS=-lppl -lgmpxx -lgmp
OBJS=polyread.o tikz-plot.o main.o 
EXECUTABLE=poly2tex

all: $(EXECUTABLE)

clean:
	rm *.o
	rm $(EXECUTABLE)

$(EXECUTABLE): $(OBJS)
	$(CXX) $(CXXFLAGS) -o $(EXECUTABLE) $(OBJS) $(LIBS)

# Creates a statically linked executable
# Useful when dealing with different versions of PPL
static: $(OBJS) 
	$(CXX) $(CXXFLAGS) -o $(EXECUTABLE)-static $(OBJS) $(TEST-OBJS) $(PPLPATH)/lib/libppl.a -lgmpxx -lgmp
