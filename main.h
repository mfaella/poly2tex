#include <ppl.hh>
#include <fstream>

using namespace Parma_Polyhedra_Library;
using namespace Parma_Polyhedra_Library::IO_Operators;
using namespace std;

typedef NNC_Polyhedron Poly;

// A possibly non-convex polyhedron
typedef Pointset_Powerset<NNC_Polyhedron> PPS;
//typedef Polyhedra_Powerset<NNC_Polyhedron> PPS;

typedef map<string,int> Varmap;
typedef map<int,float> Sectionmap;
